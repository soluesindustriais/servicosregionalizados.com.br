         <?php if(isset($categorias->getCategorias()[1])){ ?>
      <div class="container mt-100 mt-60">
         <div class="row align-items-center">
            <div class="col-lg-7 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
               <div class="section-title mr-lg-5">
                  <h4 class="title mb-4"><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1])); ?></h4>
                  <p class="text-muted">Atendimento exclusivo para região de Campinas e Minas Gerais. <br>
                  O serviço de locação de plataformas elevatórias é importante para empresas que desejam oferecer maior segurança e conforto aos profissionais que realizam o trabalho aéreo, como pinturas, limpezas e manutenções em lugares altos.</p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-categoria" class="mt-3 text-primary">Saiba Mais</a>
            </div>
         </div>
         <!--end col-->
         <div class="col-lg-5 col-md-6 order-1 order-md-2">
            <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-01.jpg" class="img-fluid shadow rounded mw-100" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1]));?>">
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <?php } ?>