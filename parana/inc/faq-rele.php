

<style type="">

.accordion-item {
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}

	.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,border-radius .15s ease;
}
.accordion-body {
    padding: 1rem 1.25rem;
}

.accordion-item:first-of-type .accordion-button {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}
.accordion-button:not(.collapsed) {
    color: #4834d4;
    background-color: #ece7ff;
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 13%);
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button:not(.collapsed)::after {
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    transform: rotate(
180deg
);
}

.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: "";
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    background-repeat: no-repeat;
    background-size: 1.25rem;
    transition: transform .2s ease-in-out;
}

.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}


table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    text-indent: initial;
    border-spacing: 2px;
    border-color: grey;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}
user agent stylesheet
table {
    border-collapse: separate;
    text-indent: initial;
    border-spacing: 2px;
}
.webanswers-webanswers_table__webanswers-table {
    overflow: hidden;
    position: relative;
    padding-top: 0;
    white-space: normal;
    padding-bottom: 20px;
}
</style>
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
 
Quais são os tipos de relé?


      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p> 
Há três tipos de relé: térmico, temporizadores e de proteção.
      </p>
<ul>  
<li class="text-dark ml-5">  
1. Relé térmicos.
É indicado para locais onde as temperaturas tendem a ser elevadas, ou seja, evita a sobrecarga de eletricidade e é ideal para aplicar em motores elétricos para não haver superaquecimento e danificar o equipamento.
</li>
<li class="text-dark ml-5">  
2. Relé temporizador. 
Ideal para o uso em operações industriais para realizar o comando de partida com o controle de tempo. Basicamente é aplicado para realizar partida de motores além do comando. 

</li>

<li class="text-dark ml-5">  
3. Relé de proteção. 
É o relé aplicado em uso de dispositivos para ligar ou desligar os aparelhos através dos campos eletromagnéticos que ocasionam mudanças de estados dos componentes.

</li>

</ul>
<p> Além disso, os relés podem ser classificados como abertos, fechados ou selados, logo, é possível fazer a utilização de acordo com suas características definidas. </p>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">


Quando usar um relé?
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p> 
        Um relé também pode ser classificado em eletromecânico e fotoelétrico. Assim, você pode usá-lo em duas situações para instalações.
        </p>
<p> 
No caso de relé eletromecânico, a parte mecânica é acionada para ocorrer a corrente elétrica na bobina, ou seja, ao acionar a bobina, atrai o eletroímã e empurra o contato A ao contato B do relé.
</p>
<p> 
Já o modelo fotoelétrico é o mais utilizado devido às funções de acionamento automático do circuito através da quantidade de luz. Um exemplo é o acionamento de lâmpadas em locais que escurecem, como nas vias públicas.
</p>

      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    
Qual a função de um relé?


      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">

        <p> 
        A principal função deste conceito é acionar ou interromper a ação de um circuito com a retransmissão da corrente elétrica. Resumidamente, funciona como um interruptor, pois tem a capacidade de fazer o isolamento de sinais, evitando curto-circuito. 
      </p>
<p> 
Desse modo, a função deste dispositivo é desarmar disjuntores de energia, evitando que a corrente elétrica gera curto-circuito e, consequentemente, incêndios ou danificação nos equipamentos.
</p>
<p> 
De forma geral, a função do relé é fechar e abrir os circuitos de acordo com os fatores ou configurações. Esse processo ocorre quando a corrente elétrica circula pela bobina, logo, é interrompida assim como o campo eletromagnético.
</p>
<p> 
Com isso, os contatos voltam às posições originais ao abrir ou fechar os circuitos, sendo uma excelente vantagem para as empresas, indústrias e residências.
</p>
<p> 
Afinal, sua função é permitir o funcionamento de outros aparelhos conectados no mesmo ou a outro circuito elétrico, como nos elevadores para a proteção contra abertura das portas, sistemas de catracas, entre diversos outros. 
</p>
      </div>
    </div>
  </div>
</div>
