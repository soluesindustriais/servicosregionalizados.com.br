    <div class="col-12 my-4">
        <h4>Regiões onde atendemos</h4>
    </div>
<div >


        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#regiao-central">Região Central</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-norte">Zona Norte</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-oeste">Zona Oeste</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-sul">Zona Sul</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-leste">Zona Leste</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#grande-sp">Grande São Paulo</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#litoral-sp">Litoral de São Paulo</a>
            </div>
        </nav>
        <div class="tab-content">
            <div class="tab-pane fade" id="regiao-central">
                <ul>
                    <li class="list-inline-item"><strong>Aclimação</strong></li>
                    <li class="list-inline-item"><strong>Bela Vista</strong></li>
                    <li class="list-inline-item"><strong>Bom Retiro</strong></li>
                    <li class="list-inline-item"><strong>Brás</strong></li>
                    <li class="list-inline-item"><strong>Cambuci</strong></li>
                    <li class="list-inline-item"><strong>Centro</strong></li>
                    <li class="list-inline-item"><strong>Consolação</strong></li>
                    <li class="list-inline-item"><strong>Higienópolis</strong></li>
                    <li class="list-inline-item"><strong>Glicério</strong></li>
                    <li class="list-inline-item"><strong>Liberdade</strong></li>
                    <li class="list-inline-item"><strong>Luz</strong></li>
                    <li class="list-inline-item"><strong>Pari</strong></li>
                    <li class="list-inline-item"><strong>República</strong></li>
                    <li class="list-inline-item"><strong>Santa Cecília</strong></li>
                    <li class="list-inline-item"><strong>Santa Efigênia</strong></li>
                    <li class="list-inline-item"><strong>Sé</strong></li>
                    <li class="list-inline-item"><strong>Vila Buarque</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-norte">
                <ul>
                    <li class="list-inline-item"><strong>Brasilândia</strong></li>
                    <li class="list-inline-item"><strong>Cachoeirinha</strong></li>
                    <li class="list-inline-item"><strong>Casa Verde</strong></li>
                    <li class="list-inline-item"><strong>Imirim</strong></li>
                    <li class="list-inline-item"><strong>Jaçanã</strong></li>
                    <li class="list-inline-item"><strong>Jardim São Paulo</strong></li>
                    <li class="list-inline-item"><strong>Lauzane Paulista</strong></li>
                    <li class="list-inline-item"><strong>Mandaqui</strong></li>
                    <li class="list-inline-item"><strong>Santana</strong></li>
                    <li class="list-inline-item"><strong>Tremembé</strong></li>
                    <li class="list-inline-item"><strong>Tucuruvi</strong></li>
                    <li class="list-inline-item"><strong>Vila Guilherme</strong></li>
                    <li class="list-inline-item"><strong>Vila Gustavo</strong></li>
                    <li class="list-inline-item"><strong>Vila Maria</strong></li>
                    <li class="list-inline-item"><strong>Vila Medeiros</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-oeste">
                <ul>
                    <li class="list-inline-item"><strong>Água Branca</strong></li>
                    <li class="list-inline-item"><strong>Bairro do Limão</strong></li>
                    <li class="list-inline-item"><strong>Barra Funda</strong></li>
                    <li class="list-inline-item"><strong>Alto da Lapa</strong></li>
                    <li class="list-inline-item"><strong>Alto de Pinheiros</strong></li>
                    <li class="list-inline-item"><strong>Butantã</strong></li>
                    <li class="list-inline-item"><strong>Freguesia do Ó</strong></li>
                    <li class="list-inline-item"><strong>Jaguaré</strong></li>
                    <li class="list-inline-item"><strong>Jaraguá</strong></li>
                    <li class="list-inline-item"><strong>Jardim Bonfiglioli</strong></li>
                    <li class="list-inline-item"><strong>Lapa</strong></li>
                    <li class="list-inline-item"><strong>Pacaembú</strong></li>
                    <li class="list-inline-item"><strong>Perdizes</strong></li>
                    <li class="list-inline-item"><strong>Perús</strong></li>
                    <li class="list-inline-item"><strong>Pinheiros</strong></li>
                    <li class="list-inline-item"><strong>Pirituba</strong></li>
                    <li class="list-inline-item"><strong>Raposo Tavares</strong></li>
                    <li class="list-inline-item"><strong>Rio Pequeno</strong></li>
                    <li class="list-inline-item"><strong>São Domingos</strong></li>
                    <li class="list-inline-item"><strong>Sumaré</strong></li>
                    <li class="list-inline-item"><strong>Vila Leopoldina</strong></li>
                    <li class="list-inline-item"><strong>Vila Sonia</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-sul">
                <ul>
                    <li class="list-inline-item"><strong>Aeroporto</strong></li>
                    <li class="list-inline-item"><strong>Água Funda</strong></li>
                    <li class="list-inline-item"><strong>Brooklin</strong></li>
                    <li class="list-inline-item"><strong>Campo Belo</strong></li>
                    <li class="list-inline-item"><strong>Campo Grande</strong></li>
                    <li class="list-inline-item"><strong>Campo Limpo</strong></li>
                    <li class="list-inline-item"><strong>Capão Redondo</strong></li>
                    <li class="list-inline-item"><strong>Cidade Ademar</strong></li>
                    <li class="list-inline-item"><strong>Cidade Dutra</strong></li>
                    <li class="list-inline-item"><strong>Cidade Jardim</strong></li>
                    <li class="list-inline-item"><strong>Grajaú</strong></li>
                    <li class="list-inline-item"><strong>Ibirapuera</strong></li>
                    <li class="list-inline-item"><strong>Interlagos</strong></li>
                    <li class="list-inline-item"><strong>Ipiranga</strong></li>
                    <li class="list-inline-item"><strong>Itaim Bibi</strong></li>
                    <li class="list-inline-item"><strong>Jabaquara</strong></li>
                    <li class="list-inline-item"><strong>Jardim Ângela</strong></li>
                    <li class="list-inline-item"><strong>Jardim América</strong></li>
                    <li class="list-inline-item"><strong>Jardim Europa</strong></li>
                    <li class="list-inline-item"><strong>Jardim Paulista</strong></li>
                    <li class="list-inline-item"><strong>Jardim Paulistano</strong></li>
                    <li class="list-inline-item"><strong>Jardim São Luiz</strong></li>
                    <li class="list-inline-item"><strong>Jardins</strong></li>
                    <li class="list-inline-item"><strong>Jockey Club</strong></li>
                    <li class="list-inline-item"><strong>M'Boi Mirim</strong></li>
                    <li class="list-inline-item"><strong>Moema</strong></li>
                    <li class="list-inline-item"><strong>Morumbi</strong></li>
                    <li class="list-inline-item"><strong>Parelheiros</strong></li>
                    <li class="list-inline-item"><strong>Pedreira</strong></li>
                    <li class="list-inline-item"><strong>Sacomã</strong></li>
                    <li class="list-inline-item"><strong>Santo Amaro</strong></li>
                    <li class="list-inline-item"><strong>Saúde</strong></li>
                    <li class="list-inline-item"><strong>Socorro</strong></li>
                    <li class="list-inline-item"><strong>Vila Andrade</strong></li>
                    <li class="list-inline-item"><strong>Vila Mariana</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-leste">
                <ul>
                    <li class="list-inline-item"><strong>Água Rasa</strong></li>
                    <li class="list-inline-item"><strong>Anália Franco</strong></li>
                    <li class="list-inline-item"><strong>Aricanduva</strong></li>
                    <li class="list-inline-item"><strong>Artur Alvim</strong></li>
                    <li class="list-inline-item"><strong>Belém</strong></li>
                    <li class="list-inline-item"><strong>Cidade Patriarca</strong></li>
                    <li class="list-inline-item"><strong>Cidade Tiradentes</strong></li>
                    <li class="list-inline-item"><strong>Engenheiro Goulart</strong></li>
                    <li class="list-inline-item"><strong>Ermelino Matarazzo</strong></li>
                    <li class="list-inline-item"><strong>Guianazes</strong></li>
                    <li class="list-inline-item"><strong>Itaim Paulista</strong></li>
                    <li class="list-inline-item"><strong>Itaquera</strong></li>
                    <li class="list-inline-item"><strong>Jardim Iguatemi</strong></li>
                    <li class="list-inline-item"><strong>José Bonifácio</strong></li>
                    <li class="list-inline-item"><strong>Moóca</strong></li>
                    <li class="list-inline-item"><strong>Parque do Carmo</strong></li>
                    <li class="list-inline-item"><strong>Parque São Lucas</strong></li>
                    <li class="list-inline-item"><strong>Parque São Rafael</strong></li>
                    <li class="list-inline-item"><strong>Penha</strong></li>
                    <li class="list-inline-item"><strong>Ponte Rasa</strong></li>
                    <li class="list-inline-item"><strong>São Mateus</strong></li>
                    <li class="list-inline-item"><strong>São Miguel Paulista</strong></li>
                    <li class="list-inline-item"><strong>Sapopemba</strong></li>
                    <li class="list-inline-item"><strong>Tatuapé</strong></li>
                    <li class="list-inline-item"><strong>Vila Carrão</strong></li>
                    <li class="list-inline-item"><strong>Vila Curuçá</strong></li>
                    <li class="list-inline-item"><strong>Vila Esperança</strong></li>
                    <li class="list-inline-item"><strong>Vila Formosa</strong></li>
                    <li class="list-inline-item"><strong>Vila Matilde</strong></li>
                    <li class="list-inline-item"><strong>Vila Prudente</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="grande-sp">
                <ul>
                    <li class="list-inline-item"><strong>São Caetano do sul</strong></li>
                    <li class="list-inline-item"><strong>São Bernardo do Campo</strong></li>
                    <li class="list-inline-item"><strong>Santo André</strong></li>
                    <li class="list-inline-item"><strong>Diadema</strong></li>
                    <li class="list-inline-item"><strong>Guarulhos</strong></li>
                    <li class="list-inline-item"><strong>Suzano</strong></li>
                    <li class="list-inline-item"><strong>Ribeirão Pires</strong></li>
                    <li class="list-inline-item"><strong>Mauá</strong></li>
                    <li class="list-inline-item"><strong>Embu</strong></li>
                    <li class="list-inline-item"><strong>Embu Guaçú</strong></li>
                    <li class="list-inline-item"><strong>Embu das Artes</strong></li>
                    <li class="list-inline-item"><strong>Itapecerica da Serra</strong></li>
                    <li class="list-inline-item"><strong>Osasco</strong></li>
                    <li class="list-inline-item"><strong>Barueri</strong></li>
                    <li class="list-inline-item"><strong>Jandira</strong></li>
                    <li class="list-inline-item"><strong>Cotia</strong></li>
                    <li class="list-inline-item"><strong>Itapevi</strong></li>
                    <li class="list-inline-item"><strong>Santana de Parnaíba</strong></li>
                    <li class="list-inline-item"><strong>Caierias</strong></li>
                    <li class="list-inline-item"><strong>Franco da Rocha</strong></li>
                    <li class="list-inline-item"><strong>Taboão da Serra</strong></li>
                    <li class="list-inline-item"><strong>Cajamar</strong></li>
                    <li class="list-inline-item"><strong>Arujá</strong></li>
                    <li class="list-inline-item"><strong>Alphaville</strong></li>
                    <li class="list-inline-item"><strong>Mairiporã</strong></li>
                    <li class="list-inline-item"><strong>ABC</strong></li>
                    <li class="list-inline-item"><strong>ABCD</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade active show" id="litoral-sp">
                <ul>
                    <li class="list-inline-item"><strong>Bertioga</strong></li>
                    <li class="list-inline-item"><strong>Cananéia</strong></li>
                    <li class="list-inline-item"><strong>Caraguatatuba</strong></li>
                    <li class="list-inline-item"><strong>Cubatão</strong></li>
                    <li class="list-inline-item"><strong>Guarujá</strong></li>
                    <li class="list-inline-item"><strong>Ilha Comprida</strong></li>
                    <li class="list-inline-item"><strong>Iguape</strong></li>
                    <li class="list-inline-item"><strong>Ilhabela</strong></li>
                    <li class="list-inline-item"><strong>Itanhaém</strong></li>
                    <li class="list-inline-item"><strong>Mongaguá</strong></li>
                    <li class="list-inline-item"><strong>Riviera de São Lourenço</strong></li>
                    <li class="list-inline-item"><strong>Santos</strong></li>
                    <li class="list-inline-item"><strong>São Vicente</strong></li>
                    <li class="list-inline-item"><strong>Praia Grande</strong></li>
                    <li class="list-inline-item"><strong>Ubatuba</strong></li>
                    <li class="list-inline-item"><strong>São Sebastião</strong></li>
                    <li class="list-inline-item"><strong>Peruíbe</strong></li>
                </ul>
            </div>

        </div>
<br>
   <br>
   <br>
    </div>