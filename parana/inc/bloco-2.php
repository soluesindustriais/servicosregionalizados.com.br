
<div class="container-fluid funtoSearch my-sm-3 pb-5">
   <div class="row container mx-auto ">    
<h2 class="col-12 w-100 text-center py-5">Vantagens</h2>
         <div class="col-md-4 col-12">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/user.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Cote com diversas empresas</h4>
                  <p class="text-muted mb-0">Solicite orçamentos com vários fornecedores ao mesmo tempo e em poucos cliques.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/calendar.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Dezenas de cotações diariamente</h4>
                  <p class="text-muted mb-0">Solicite orçamento com dezenas de fornecedores qualificados de forma simples.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/sand-clock.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Economize tempo</h4>
                  <p class="text-muted mb-0">Otimize seu tempo e selecione os melhores fornecedores.</p>
               </div>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->