    <div class="col-12 my-4 w-100">
        <h4>Regiões onde atendemos</h4>
    </div>
<div class="w-100 col-12">
     

        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#parana">PARANÁ </a>
                <a class="nav-item nav-link" data-toggle="tab" href="#santa-catarina">SANTA CATARINA </a>
                <a class="nav-item nav-link" data-toggle="tab" href="#rio-grande-do-sul">RIO GRANDE DO SUL</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#mato-grosso-do-sul">MATO GROSSO DO SUL</a>
            </div>
        </nav>
        <div class="tab-content w-100">
            <div class="tab-pane fade active show" id="parana">
                <ul>
                    <li class="list-inline-item"><strong>Curitiba</strong></li>
                    <li class="list-inline-item"><strong>Londrina</strong></li>
                    <li class="list-inline-item"><strong>Maringá</strong></li>
                    <li class="list-inline-item"><strong>Foz do Iguaçu</strong></li>
                    <li class="list-inline-item"><strong>Guarapuava</strong></li>
                    <li class="list-inline-item"><strong>Cascavel</strong></li>
                    <li class="list-inline-item"><strong>Toledo</strong></li>
                    <li class="list-inline-item"><strong>Quatro Pontes</strong></li>
                    <li class="list-inline-item"><strong>Cianorte</strong></li>

                </ul>
            </div>
            <div class="tab-pane fade" id="santa-catarina">
                <ul>
                    <li class="list-inline-item"><strong>Joinville</strong></li>
                    <li class="list-inline-item"><strong>Florianópolis</strong></li>
                    <li class="list-inline-item"><strong>Blumenau</strong></li>
                    <li class="list-inline-item"><strong>São José</strong></li>
                    <li class="list-inline-item"><strong>Chapecó</strong></li>
                    <li class="list-inline-item"><strong>Itajaí</strong></li>
                    <li class="list-inline-item"><strong>Criciúma</strong></li>
                    <li class="list-inline-item"><strong>Jaraguá do Sul</strong></li>
                    <li class="list-inline-item"><strong>Palhoça</strong></li>
                    <li class="list-inline-item"><strong>Lages</strong></li>


                </ul>
            </div>
            <div class="tab-pane fade" id="rio-grande-do-sul">
                <ul>
                    <li class="list-inline-item"><strong>Porto Alegre</strong></li>
                    <li class="list-inline-item"><strong>Caxias do Sul</strong></li>
                    <li class="list-inline-item"><strong>Canoas</strong></li>
                    <li class="list-inline-item"><strong>Pelotas</strong></li>
                    <li class="list-inline-item"><strong>Santa Maria</strong></li>
                    <li class="list-inline-item"><strong>Gravataí</strong></li>
                    <li class="list-inline-item"><strong>Viamão</strong></li>
                    <li class="list-inline-item"><strong>Novo Hamburgo</strong></li>

                </ul>
            </div>
            <div class="tab-pane fade" id="mato-grosso-do-sul">
                <ul>
                   <li class="list-inline-item"><strong>Campo Grande    </strong></li>
                   <li class="list-inline-item"><strong>Dourados</strong></li>
                   <li class="list-inline-item"><strong>Três Lagoas</strong></li>
                   <li class="list-inline-item"><strong>Corumbá</strong></li>
                   <li class="list-inline-item"><strong>Ponta Porã</strong></li>
                   <li class="list-inline-item"><strong>Sidrolândia</strong></li>
                   <li class="list-inline-item"><strong>Naviraí</strong></li>
                   <li class="list-inline-item"><strong>Nova Andradina  </strong></li>
                   <li class="list-inline-item"><strong>Aquidauana</strong></li>
                   <li class="list-inline-item"><strong>Maracaju</strong></li>
                </ul>
            </div>

        
<br>
   <br>
    </div>
   <br>
   <br>