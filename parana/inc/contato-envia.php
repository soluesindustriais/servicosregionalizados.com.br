<?php 
date_default_timezone_set("America/Sao_Paulo");
// Verifico se foi feita a postagem do Captcha

    //Atenticador do e-mail com SSL
    require_once('inc/contato/mail.send.php');

    //Informações que serão gravadas no isereleads
    $recebenome = $post["nome"];
    $recebemail = $post["email"];
    $recebetelefone = $post["telefone"];
    $recebemensagem = strip_tags(trim($post["mensagem"]));
    $dataEnvio = date('d/m/Y H:i:s');

    // MENSAGEM 
    $corpo = null;
    $corpo .= "<table style='border-collapse:collapse;border-spacing:0;border-color:#761919'>
              <tr>
                <th style='font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;vertical-align:top;text-align: center;' colspan='2'>
                  <a href='{$url}' title='{$nomeSite}'><img src='{$url}/imagens/logo.png' width='85' title='{$nomeSite}' alt='{$nomeSite}'></a>
                </th>
              </tr>
              
              <tr>
                <th style='font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px;vertical-align:top;text-align: center;' colspan='2'>
                  Mensagem recebida de {$recebenome}, via formulário do site.
                </th>
              </tr>
              
              <tr>";
    foreach ($post as $key => $value):
      $corpo .= "<tr>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top;border-right:1px solid #ccc;'>
                <b>" . strtoupper(str_replace(array('_', '-'), ' ', $key)) . ": </b>
              </td>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top'>
                {$value}
              </td>
              </tr>";
    endforeach;
    $corpo .= "<tr>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top;border-right:1px solid #ccc;'>
                <b>" . strtoupper(str_replace(array('_', '-'), ' ', 'PRODUTO')) . ": </b>
              </td>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top'>
                {$h1}
              </td>
              </tr>";   
    $corpo .= "</tr>   
              <tr>
                <td style='text-align:center;font-family:Arial, sans-serif;font-size:9px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;text-align:center;vertical-align:top' colspan='2'>
                  Mensagem automática enviada por - {$nomeSite} em {$dataEnvio}
                </td>
              </tr>
              <tr>
                <td style='text-align:center;font-family:Arial, sans-serif;font-size:9px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;text-align:center;vertical-align:top' colspan='2'>
                  <a href='{$url}' title='{$nomeSite}'>{$url}</a>
                  <p style='font-size:10px;font-weight:bold;'>Em breve as melhores empresas especialidadas em {$h1} entrara em contato! Fique ligado =)</p>
                </td>
              </tr>
            </table>";

    //ENVIO EMPRESA
    $mail->From = $EMAIL; // Remetente
    $mail->FromName = $post['nome']; // Remetente nome
    $mail->Sender = $EMAIL; // Seu e-mail

    $mail->AddAddress('kaue.dias@idealtrends.com.br', 'Kauê'); // Destinatário principal
    //$mail->AddCC('milena.martins@solucoesindustriais.com.br', 'Milena Martins');
    //$mail->AddCC('guilherme.prado@solucoesindustriais.com.br', 'Guilherme Prado');
    //    $mail->AddCC('isys.costa@idealtrends.com.br', 'Isys Costa');
    //$mail->AddCC('tacio.fonseca@solucoesindustriais.com.br', 'Tacio Fonseca');
    //$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
    $mail->AddReplyTo($post['email'], $post['nome']); // Reply-to
    $mail->Subject = $EMPRESA . ': Soluções Industriais - Landing Page'; // Assunto da mensagem
    $mail->Body = $corpo; // corpo da mensagem
    $enviaEmpresa = $mail->Send(); // Enviando o e-mail
    $mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos
    
    // ENVIO USUÁRIO
    $mail->From = $recebemail; // Remetente
    $mail->FromName = $EMPRESA; // Remetente nome
    $mail->Sender = $EMAIL; // Seu e-mail
    $mail->AddAddress($post['email'], $post['nome']); // Destinatário principal

    $mail->Subject = $EMPRESA . ': Recebemos sua mensagem'; // Assunto da mensagem
    $mail->Body = $corpo; // corpo da mensagem
    $mail->Send(); // Enviando o e-mail
    $mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos

    // echo '<pre>';
    // print_r($mail);
    // echo '</pre>';
    //  exit;
    // if ($enviaEmpresa):
    //   echo '<script>';
    //   echo ''
    //   . '$(function () {';
    //   echo 'swal({
    //         title: "Tudo certo!",
    //         text: "Obrigado por entrar em contato, sua mensagem foi enviada com sucesso",
    //         type: "success",
    //         icon: "success",
    //         showCancelButton: false,
    //         confirmButtonClass: "btn-success",
    //         confirmButtonText: "Ok",
    //         closeOnConfirm: true
    //       });

    //       $(".swal-button--confirm").attr("onclick","location.href=\'https://www.solucoesindustriais.com.br/lps/certificacao-de-qualificacao-de-soldadores/obrigado-pela-preferencia.php\'");
    //       $("form input").each(function(){
    //     $(this).val("");
    //       });';
    //   echo '});'
    //   . '</script>';
    // else:
    //   echo '<script>'
    //   . '$(function () {';
    //   echo 'swal("Opsss!", "Desculpe, mas houve um erro ao enviar a mensagem, por favor tente novamente.", "error");';
    //   echo '});'
    //   . '</script>';
    // endif;
    

 