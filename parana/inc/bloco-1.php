
<? include('inc/vetDestaque.php'); ?>
<section class="">
   <div class="container-fluid m-0 p-0 d-none d-sm-flex">
   <div class="funtoSearch m-0 p-5">
   <h2 class="pt-5 text-center">Encontre as <span
                     class="text-primary font-weight-bold">melhores empresas</span>, tudo em um <span
                     class="text-primary font-weight-bold">único lugar!</span></h2>
   <h3 class="pb-2 text-center mt-3"><span class="text-primary font-weight-bold">Busque</span> facilmente o que procura!</h3>
      <div class="container mb-5 ">
         
   <? include( 'inc/search-box.php' ); ?>
      </div>
   </div>
</div>
<br>
      <div class="row justify-content-center">
        
            <div class="section-title mt-5 mw-100 text-center mw-100">
               <h4 class="title mb-4 ">Conheça nossos produtos</h4>
               <p class="text-muted para-desc mb-0">Receba cotação de
               <? 
                  $i = 0; 
                  $numCategorias = count($categorias->getCategorias())-1; 
                  foreach($categorias->getCategorias() as $categoria):
                     $categoriaSemAcento = $trata->trataAcentos($categoria);
                     $categoriaSemHifen = $trata->retiraHifen($categoria);
                  ?>              
                  <? if ( $i < $numCategorias ): ?>
                     <a href="<?=$categoriaSemAcento."-categoria"; ?>" class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?>, </a> 
                  <? else: ?>
                     <a href="<?=$categoriaSemAcento."-categoria"; ?>" class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?></a> 
                  <? endif; ?>
               <? $i++; endforeach; ?>
               e muito mais. </p>
            </div>
         
         <!--end col-->
      </div>   
   <div class="container">
      <div class="row">
         <div class="col-12 mb-4 pt-2">
            <div id="customer-testi" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">
                     <? foreach($vetDestaque as $palavras => $palavra): ?>
                     <div style="height: 350px; max-height: 100%;" class="owl-item overflow-hidden  products-carousel-home">
                        <a href="<?= $trata->trataAcentos(($palavra['palavra'])); ?>">
                           <div class="card  border-0 text-center  bg-transparent">
                              <img class="w-100 mw-100 " src="imagens/home/destaque/<?= $trata->trataAcentos(($palavra['palavra'])); ?>.jpg" alt="<?=$trata->retiraHifen($palavra['palavra']);?>">
                             <div class="fundo-card">
                                
                                <!--  <p class="d-none text-dark mt-1"><?=$palavra['texto'];?></p> -->
                                 <h3 class="text-primary text-center mt-3"><?=$trata->retiraHifen($palavra['palavra']);?></h3>
                             </div>
                             
                           </div>
                        </a>
                     </div>
                     <? endforeach; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container mb-sm-5 py-sm-5">
      <div class="row align-items-center">
         <div class="col-lg-6 col-md-6">
            <img src="imagens/regiao-gif.gif" class="img-fluid shadow rounded mw-100" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0]));?>">
         </div>
         <!--end col-->
         <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 py-5 funtoCat">
            <div class="section-title ml-lg-5">
               <h4 class="title mb-4 text-light "><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0])); ?></h4>
               <p class="text-light">Atendimento exclusivo para região de Campinas e Minas Gerais. <br>
                  O serviço de locação de plataformas elevatórias é importante para empresas que desejam oferecer maior segurança e conforto aos profissionais que realizam o trabalho aéreo, como pinturas, limpezas e manutenções em lugares altos.</p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-categoria" class="mt-3 text-primary">Saiba Mais </a>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->

</section>  