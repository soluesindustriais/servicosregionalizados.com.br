    <div class="col-12 my-4">
        <h4>Regiões onde atendemos</h4>
    </div>
<div >


        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#parana">Paraná</a>
            </div>
        </nav>
        <div class="tab-content">
        
            <div class="tab-pane fade active show" id="parana">
                <ul>
                    <li class="list-inline-item"><strong>Joinville</strong></li>
                    <li class="list-inline-item"><strong>Florianópolis</strong></li>
                    <li class="list-inline-item"><strong>Blumenau</strong></li>
                    <li class="list-inline-item"><strong>São José</strong></li>
                    <li class="list-inline-item"><strong>Chapecó</strong></li>
                    <li class="list-inline-item"><strong>Itajaí</strong></li>
                    <li class="list-inline-item"><strong>Criciúma</strong></li>
                    <li class="list-inline-item"><strong>Jaraguá do Sul</strong></li>
                    <li class="list-inline-item"><strong>Palhoça</strong></li>
                    <li class="list-inline-item"><strong>Lages</strong></li>
                    <li class="list-inline-item"><strong>Balneário Camboriú </strong></li>
                    <li class="list-inline-item"><strong>Brusque</strong></li>
                    <li class="list-inline-item"><strong>Tubarão</strong></li>
                    <li class="list-inline-item"><strong>São Bento do Sul</strong></li>
                    <li class="list-inline-item"><strong>Camboriú</strong></li>
                    <li class="list-inline-item"><strong>Navegantes</strong></li>
                    <li class="list-inline-item"><strong>Caçador </strong></li>
                    <li class="list-inline-item"><strong>Concórdia</strong></li>
                    <li class="list-inline-item"><strong>Rio do Sul</strong></li>
                    <li class="list-inline-item"><strong>Indaial </strong></li>
                    <li class="list-inline-item"><strong>Gaspar  </strong></li>
                    <li class="list-inline-item"><strong>Biguaçu </strong></li>
                    <li class="list-inline-item"><strong>Araranguá   </strong></li>
                    <li class="list-inline-item"><strong>Itapema </strong></li>
                    <li class="list-inline-item"><strong>Içara </strong></li>
                    <li class="list-inline-item"><strong>Mafra</strong></li>
                    <li class="list-inline-item"><strong>Canoinhas</strong></li>
                    <li class="list-inline-item"><strong>São Francisco do Sul  </strong></li>
                    <li class="list-inline-item"><strong>Videira</strong></li>
                    <li class="list-inline-item"><strong>Xanxerê</strong></li>
              </ul            </div
        </div>
<br>
   <br>
   <br>
    </div>
