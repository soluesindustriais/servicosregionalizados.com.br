    <div class="col-12 my-4">
        <h4>Regiões onde atendemos</h4>
    </div>
<div >


        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#parana">Paraná</a>
            </div>
        </nav>
        <div class="tab-content">
        
            <div class="tab-pane fade active show" id="parana">
                <ul>
                    <li class="list-inline-item"><strong>Campo Grande</strong></li>
                    <li class="list-inline-item"><strong>Dourados</strong></li>
                    <li class="list-inline-item"><strong>Três Lagoas</strong></li>
                    <li class="list-inline-item"><strong>Corumbá</strong></li>
                    <li class="list-inline-item"><strong>Ponta Porã</strong></li>
                    <li class="list-inline-item"><strong>Sidrolândia</strong></li>
                    <li class="list-inline-item"><strong>Naviraí</strong></li>
                    <li class="list-inline-item"><strong>Nova Andradina</strong></li>
                    <li class="list-inline-item"><strong>Aquidauana</strong></li>
                    <li class="list-inline-item"><strong>Maracaju</strong></li>
                    <li class="list-inline-item"><strong>Paranaíba</strong></li>
                    <li class="list-inline-item"><strong>Amambai</strong></li>
                    <li class="list-inline-item"><strong>Rio Brilhante</strong></li>
                    <li class="list-inline-item"><strong>Coxim </strong></li>
                    <li class="list-inline-item"><strong>Caarapó </strong></li>
                    <li class="list-inline-item"><strong>Miranda</strong></li>
                    <li class="list-inline-item"><strong>São Gabriel do Oeste</strong></li>
                    <li class="list-inline-item"><strong>Jardim</strong></li>
                    <li class="list-inline-item"><strong>Aparecida do Taboado</strong></li>
                    <li class="list-inline-item"><strong>Chapadão do Sul</strong></li>
                    <li class="list-inline-item"><strong>Anastácio</strong></li>
                    <li class="list-inline-item"><strong>Itaporã</strong></li>
                    <li class="list-inline-item"><strong>Ribas do Rio Pardo</strong></li>
                    <li class="list-inline-item"><strong>Bela Vista</strong></li>
                    <li class="list-inline-item"><strong>Ladário</strong></li>
                    <li class="list-inline-item"><strong>Bataguassu</strong></li>
                    <li class="list-inline-item"><strong>Ivinhema</strong></li>
                    <li class="list-inline-item"><strong>Nova Alvorada do Sul</strong></li>
                    <li class="list-inline-item"><strong>Terenos</strong></li>
                    <li class="list-inline-item"><strong>Bonito</strong></li>
              </ul            </div
        </div>
<br>
   <br>
   <br>
    </div>
