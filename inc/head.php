<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/Thing" lang="pt-br">
<head>
    <meta charset="utf-8">
    <? include('inc/geral.php'); ?>
    
    <link rel="preload" href="css/style.css" as="style">
    <link rel="stylesheet" href="css/style.css">

    <link rel="preconnect" href="https://use.fontawesome.com">
    <link 
        rel="stylesheet" 
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"
    >
<script src="js/jquery-3.4.1.min.js"></script>


    <link rel="preconnect" href="https://fonts.googleapis.com/">
</head>
