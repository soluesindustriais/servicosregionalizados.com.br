

<style type="">

.accordion-item {
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}

	.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,border-radius .15s ease;
}
.accordion-body {
    padding: 1rem 1.25rem;
}

.accordion-item:first-of-type .accordion-button {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}
.accordion-button:not(.collapsed) {
    color: #4834d4;
    background-color: #ece7ff;
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 13%);
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button:not(.collapsed)::after {
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    transform: rotate(
180deg
);
}

.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: "";
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    background-repeat: no-repeat;
    background-size: 1.25rem;
    transition: transform .2s ease-in-out;
}

.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}


table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    text-indent: initial;
    border-spacing: 2px;
    border-color: grey;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}

table {
    border-collapse: separate;
    text-indent: initial;
    border-spacing: 2px;
}
.webanswers-webanswers_table__webanswers-table {
    overflow: hidden;
    position: relative;
    padding-top: 0;
    white-space: normal;
    padding-bottom: 20px;
}
</style>
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
 
O que é o disjuntor motor?


      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <p>       
        O disjuntor motor, nada mais é do que um dispositivo de proteção e controle para motores elétricos. Ele proporciona aos aparelhos segurança e proteção contra curtos e sobrecargas de energia  
      </p>
<p>
  
Esse componente também proporciona um controle sobre os motores, já que possui uma chave para manobras e ações sobre ele. Outra grande funcionalidade desse dispositivo é a sua capacidade de lidar com a corrente de partida de um motor elétrico. 
</p>
<p>
  
Ao dar partida em um motor, há um grande pico de corrente para tirar o motor do estado de repouso e colocá-lo em movimento, como esse processo demanda mais energia o disjuntor controla a corrente gerada para que o motor não entre em colapso.
</p>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">


Como funciona o disjuntor motor?
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p>
  
Um disjuntor motor se assemelha a um disjuntor comum, oferecendo a proteção magnética e térmica contra possíveis falhas de energia e sobrecargas. 
</p>
<p>
  
Dentro desse dispositivo existe uma bobina que quando gerada uma corrente elétrica maior que a permitida, seu campo eletromagnético desloca seus componentes impedindo essa condução. 
</p>
<p>
  
Para evitar essas sobrecargas e curtos-circuitos, além da bobina o disjuntor também conta com lâminas de contato que se dilatam com calor. 
</p>
<p>
  
Dessa forma, quando há uma sobrecorrente, o calor gerado faz com que as lâminas de contato se dilatem, até que saiam da posição inicial de contato.Assim, impedindo a passagem da corrente elétrica.  
</p>
<p>
  
Os disjuntores motores, geralmente possuem 3 terminais de cada lado, para receber as fases que vão alimentar o motor elétrico. 
</p> 
<p>
  
Ao utilizar esse componente, é fundamental realizar um ajuste para a corrente nominal e corrente de partida do motor em que ele será utilizado, evitando possíveis falhas. 
</p>

      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    

Vantagens do disjuntor motor?

      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">

<p>
  
Algumas das vantagens são que os disjuntores motores asseguram total proteção ao motor e ao circuito elétrico com seus disparadores térmico e magnético, em alguns modelos é possível integrar contatos auxiliares, caso necessário 
</p>        
<p>
  
Os disjuntores motores, também não necessitam de peças de reposição, como no caso de fusíveis por exemplo. Isso porque contam com uma alta capacidade de interrupção, que faz com que sejam usados em locais com altas correntes de energia.
</p>
<p>
  
Dessa forma podem ser religados facilmente depois de alguma falha ou mesmo durante alguma manutenção que esteja sendo feita. 
</p>

      </div>
    </div>
  </div>
</div>
