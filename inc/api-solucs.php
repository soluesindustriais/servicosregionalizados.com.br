<!-- Shark Orcamento -->

<div id="sharkOrcamento" style="display: none;"></div>

<script>
    var guardar = document.querySelectorAll('.botao-cotar');
    for (var i = 0; i < guardar.length; i++) {
        guardar[i].removeAttribute('href');
        var adicionando = guardar[i].parentNode;
        adicionando.classList.add('nova-api');
    };
</script>

<app-cotacao-solucs class="api-solucs"
    appConfig='{"btnOrcamento": ".botao-cotar", "titulo": "h2", "industria": "solucoes-industriais", "container":"nova-api"}'>
</app-cotacao-solucs>
<link rel="stylesheet" href="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/styles.css">
<script
    src="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/package-es5.js?v=<?= date_timestamp_get(date_create()); ?>"
    nomodule defer></script>
<script
    src="https://sdk.solucoesindustriais.com.br/dist/sdk-cotacao-solucs/package-es2015.js?v=<?= date_timestamp_get(date_create()); ?>"
    type="module" defer></script>


<script>
    $("#btnOrcamento").click(function () {
        $('html,body').animate({
            scrollTop: $(".formulario-lado").offset().top - 125
        }, 'slow');
    });

    // $(document).ready(function validaAltura(){
    //     var validaAltura = $(".validaAltura").outerHeight()

    //     if (validaAltura >= 250 ) { 
    //        $('.btn-leia').css('display','block'); 
    //     }
    //     else { 
    // $('.btn-leia').css('display','none');  
    //     } 
    // });
</script>

<script src="https://www.solucoesindustriais.com.br/assets/js/plugin/telefones.min.js"></script>

<script>
    pluginTelefonesJs.init({
        industria: 'solucoes-industriais',
        btnTelefone: '#btnTelefone',
        titulo: 'h2'
    });
</script>

<!-- Shark Orcamento end -->