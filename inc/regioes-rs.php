    <div class="col-12 my-4">
        <h4>Regiões onde atendemos</h4>
    </div>
<div >


        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#parana">Rio grande do Sul</a>
            </div>
        </nav>
        <div class="tab-content">
        
            <div class="tab-pane fade active show" id="parana">
                <ul>
                    <li class="list-inline-item"><strong>Porto Alegre</strong></li>
                    <li class="list-inline-item"><strong>Caxias do Sul</strong></li>
                    <li class="list-inline-item"><strong>Canoas</strong></li>
                    <li class="list-inline-item"><strong>Pelotas</strong></li>
                    <li class="list-inline-item"><strong>Santa Maria</strong></li>
                    <li class="list-inline-item"><strong>Gravataí</strong></li>
                    <li class="list-inline-item"><strong>Viamão</strong></li>
                    <li class="list-inline-item"><strong>Novo Hamburgo</strong></li>
                    <li class="list-inline-item"><strong>São Leopoldo</strong></li>
                    <li class="list-inline-item"><strong>Rio Grande</strong></li>
                    <li class="list-inline-item"><strong>Alvorada</strong></li>
                    <li class="list-inline-item"><strong>Passo Fundo</strong></li>
                    <li class="list-inline-item"><strong>Sapucaia do Sul</strong></li>
                    <li class="list-inline-item"><strong>Santa Cruz do Sul</strong></li>
                    <li class="list-inline-item"><strong>Cachoeirinha</strong></li>
                    <li class="list-inline-item"><strong>Bento Gonçalves</strong></li>
                    <li class="list-inline-item"><strong>Bagé</strong></li>
                    <li class="list-inline-item"><strong>Erechim</strong></li>

              </ul>            </div>
        </div>
<br>
   <br>
   <br>
    </div>
