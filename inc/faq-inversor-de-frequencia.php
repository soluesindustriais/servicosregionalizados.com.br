

<style type="">

.accordion-item {
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}

	.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,border-radius .15s ease;
}
.accordion-body {
    padding: 1rem 1.25rem;
}

.accordion-item:first-of-type .accordion-button {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}
.accordion-button:not(.collapsed) {
    color: #4834d4;
    background-color: #ece7ff;
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 13%);
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button:not(.collapsed)::after {
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    transform: rotate(
180deg
);
}

.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: "";
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    background-repeat: no-repeat;
    background-size: 1.25rem;
    transition: transform .2s ease-in-out;
}

.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}


table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    text-indent: initial;
    border-spacing: 2px;
    border-color: grey;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}
user agent stylesheet
table {
    border-collapse: separate;
    text-indent: initial;
    border-spacing: 2px;
}
.webanswers-webanswers_table__webanswers-table {
    overflow: hidden;
    position: relative;
    padding-top: 0;
    white-space: normal;
    padding-bottom: 20px;
}
</style>
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Como funciona o inversor frequência?


      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p> 
Um inversor de frequência é um aparelho eletrônico que altera as velocidades de rotação de um motor elétrico. Controlando suas correntes de frequência e as saídas de energia. 


      </p>

<p> O inversor de frequência possui vários estágios de funcionamento, o primeiro deles é por onde passam as correntes de energia, vindas da rede elétrica e as convertendo em corrente contínua.  

 </p>
 <p>  
Esse processo é realizado por meio de um conversor composto por seis diodos, que são pequenos dispositivos que se assemelham a válvulas de retenção. Desta forma, permitindo que a corrente vá na direção certa. Após passar pelo conversor, a tensão nas fases é contínua pulsante. 


 </p>
 <p>  
 O estágio seguinte do sistema, é o barramento da corrente contínua. Esse processo é possível por conta de filtros capacitores, capazes de captar somente os sinais pulsantes do dispositivo.

</p>
<p> 
Sendo assim, o sinal pulsante torna-se novamente contínuo, mas com ondulações mínimas. Realizado assim a função principal do inversor de frequência, que é a de controlar as correntes fornecidas pela rede elétrica. 
</p>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">


Vantagens de usar um inversor de frequência?
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p> 
        A primeira vantagem de utilizar um inversor de frequência, é a redução no consumo e nos custos de energia. Se você possui um sistema que não precisa ser operado em velocidade máxima a todo instante, pode ser uma grande vantagem.


        </p>
<p> 
Isso porque o inversor de frequência permite que você combine a velocidade do motor elétrico junto com a carga de energia que é necessária para o funcionamento dele, evitando possíveis sobrecargas.


</p>
<p> 
Outra vantagem é que com o inversor de frequência é possível aumentar a produção e ter um maior controle sobre os processos necessários, o motivo disso é que os motores irão operar com a velocidade adequada para sua aplicação, reduzindo erros e desgastes. 


</p>
<p> 
Por fim, utilizando esse dispositivo você é capaz de aumentar a vida útil do seu equipamento e reduzir gastos com manutenção. Isso porque o inversor de frequência garante a velocidade de aplicação ideal para seu motor elétrico oferecendo uma maior proteção. 
</p>

      </div>
    </div>
  </div>
</div>
