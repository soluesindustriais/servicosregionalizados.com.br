<!-- Add Lighbox to all images init -->
<script>
    const imgLightbox = document.querySelectorAll('a:not([title="logo"]) img');
    const addLightbox = (item) => 
    {
        item.parentElement.classList.add('lightbox');
    };

    imgLightbox.forEach(item => addLightbox(item));
</script>
<!-- Add Lighbox to all images end -->