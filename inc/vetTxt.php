<?php
$vetTxt = array();

$vetTxt[1] = array("id" => "1" , "text" =>"Apesar de serem produtos frequentemente usados no nosso dia a dia e presente em praticamente todas as atividades, ainda existem muitas dúvidas em relação às diferenças entre cabos e fios, o que é comum, considerando a semelhança entre ambos....");
$vetTxt[2] = array("id" => "2" , "text" =>"A automação industrial é o nome dado ao processo que moderniza máquinas e processos de montagem nas empresas com o intuito de dar mais velocidade ao trabalho. Ao aderir à automação, algumas tarefas podem ser feitas automaticamente, e os colaboradores antes responsáveis por elas podem dedicar seu tempo e esforço a outras atividades....");
$vetTxt[3] = array("id" => "3" , "text" =>"Servo motores é um equipamento tecnológico fundamental para ser aplicado nos processos industriais e organizacionais, pois contribui com a alta capacidade em girar ou empurrar objetos com eficiência....");
$vetTxt[4] = array("id" => "4" , "text" =>"O controlador de fator de potência é um equipamento de gestão de energia elétrica, que pode ser utilizado em qualquer lugar, como por exemplo em indústrias, residências, pequenas empresas, entre outros. Permitido o melhor consumo de energia no local...");
$vetTxt[5] = array("id" => "5" , "text" =>"O uso de relé é indispensável para ser aplicado nas redes elétricas, afinal, é a base para modificar circuitos elétricos de saída através do comando e alimentação de uma corrente elétrica....");
$vetTxt[6] = array("id" => "6" , "text" =>"O inversor de frequência é um aparelho que tem como sua principal função, acionar um motor elétrico por meio de um controle de frequência fornecido pela rede elétrica....");
$vetTxt[7] = array("id" => "7" , "text" =>"As instalações elétricas industriais são a união de componentes elétricos, essenciais para o bom funcionamento e a segurança dos circuitos de uma empresa....");
$vetTxt[8] = array("id" => "8" , "text" =>"A fonte de alimentação é uma das partes mais importantes de um dispositivo eletrônico, esse equipamento é utilizado para alimentar cargas elétricas de um elemento para o outro....");
$vetTxt[9] = array("id" => "9" , "text" =>"O disjuntor motor é um dispositivos muito utilizados em projetos de máquinas, acionamentos industriais e também em motores elétricos. Esse aparelho traz maior proteção e controle para os equipamentos....");
$vetTxt[10] = array("id" => "10" , "text" =>"O uso de relé é indispensável para ser aplicado nas redes elétricas, afinal, é a base para modificar circuitos elétricos de saída através do comando e alimentação de uma corrente elétrica....");

?>