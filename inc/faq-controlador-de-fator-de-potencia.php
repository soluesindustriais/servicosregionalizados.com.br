

<style type="">

.accordion-item {
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}

	.accordion-button {
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 1rem 1.25rem;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    background-color: #fff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out,border-radius .15s ease;
}
.accordion-body {
    padding: 1rem 1.25rem;
}

.accordion-item:first-of-type .accordion-button {
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}
.accordion-button:not(.collapsed) {
    color: #4834d4;
    background-color: #ece7ff;
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 13%);
}
.accordion-header {
    margin-bottom: 0;
}
.accordion-button:not(.collapsed)::after {
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    transform: rotate(
180deg
);
}

.accordion-button::after {
    flex-shrink: 0;
    width: 1.25rem;
    height: 1.25rem;
    margin-left: auto;
    content: "";
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23212529'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e);
    background-repeat: no-repeat;
    background-size: 1.25rem;
    transition: transform .2s ease-in-out;
}

.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}


table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    text-indent: initial;
    border-spacing: 2px;
    border-color: grey;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.webanswers-webanswers_table__webanswers-table table {
    table-layout: fixed;
    width: 100%;
    border-collapse: collapse;
}
user agent stylesheet
table {
    border-collapse: separate;
    text-indent: initial;
    border-spacing: 2px;
}
.webanswers-webanswers_table__webanswers-table {
    overflow: hidden;
    position: relative;
    padding-top: 0;
    white-space: normal;
    padding-bottom: 20px;
}
</style>
<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
 


O que é fator de potência?





      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p> 

Antes de falar o que são os controladores de fator de potência, é necessário entender primeiro o que é o fator de potência.
</p>
<p> 
Resumidamente podemos definir o fator de potência como, a medida de quanto da potência da energia elétrica está sendo consumida em atividades úteis, sem desperdício. 
</p>
<p> 
O mínimo permitido de fator de potência na conta de energia, segundo a Agência Nacional de Energia Elétrica (ANEEL), é de 0,92. Se o valor estiver abaixo disso, a concessionária fornecedora de energia, pode legalmente te cobrar uma multa.
</p>
<p> 
É importante ressaltar, que tudo que exige energia reativa elevada acaba causando baixo fator de potência, alguns exemplo são: motores trabalhando em vazio durante longos períodos, transformadores alimentando pequenas cargas por muito tempo e lâmpadas de descarga (fluorescentes, com vapor de mercúrio o de sódios).
</p>

      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

<h2>  
Como funciona o controlador de fator de potência?
</h2>
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p> 
Como citamos acima, os controladores de fator de potência servem para regular as energias geradas.
</p>
<p> 
O controlador de fator de potência trabalha por meio de um sistema de monitoramento de tensão, que permite observar especificações mais precisas no consumo de energia. 
</p>
<p> 
Com isso, diminuindo o consumo desnecessário das redes elétricas. Além disso, contam com diversas configurações para se adequar da melhor forma a cada sistema de distribuição de energia.
</p>
<p> 
Mediante a essa melhor gestão no consumo de energia, o controlador de fator de potência proporciona uma maior economia e um consumo mais eficiente, destinando a carga de energia necessária para os lugares. 
</p>
<p> 
Após o seu processo de instalação e configuração, o controlador de fator de potência pode ser operado de forma automática, sem que seja necessário um acompanhamento.
</p>
<p> 
No entanto, para que isso ocorra sem problemas, é necessário que aconteçam as  manutenções preventivas e revisões das configurações em intervalos regulares. 
</p>
<p> 
Assim, trazendo o máximo de eficiência no uso do controlador de fator de potência, com segurança na rede elétrica e atendendo as necessidades do consumidor.  
</p>

      </div>
    </div>
  </div>
</div>

